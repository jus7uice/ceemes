<?php
namespace App;
use Schema;
use DB;
use Hash;

class _Seeds {

	function __construct() {	
		// Create default tables
		$this->_insert_table_admin();
		$this->_insert_table_admin_group();
		$this->_insert_table_setting();
	}

	/* Tbl Admin */
	function _insert_table_admin()
	{
		if(Schema::hasTable('admin')){
			if(DB::table('admin')->count() <= 0){
				DB::table('admin')->insert([
					"admin_group_id" =>	1,
					"username" =>	'admin',
					"password" =>	Hash::make('admin'),
					"name" =>	'Super Admin',
					"email" =>	'',
					"is_superadmin" =>	1,
					"recovery_token" =>	md5(time()),
					"created_at" =>	date("Y-m-d H:i:s"),
					"updated_at" =>	date("Y-m-d H:i:s"),
					"status" =>	1,
				]);
			}
		}
	}
	
	/* Admin Group */
	function _insert_table_admin_group()
	{
		if(Schema::hasTable('admin_group')){
			if(DB::table('admin_group')->count() == 0){
				DB::table('admin_group')->insert([
					"name" 				=>	'Super Admin',
					"parent" 			=>	0,
					"restrical_access" => '[]',
					"params" => '[]',
					"created_at" =>	date("Y-m-d H:i:s"),
					"updated_at" =>	date("Y-m-d H:i:s"),
					"status" =>	1,
				]);
			}
		}
	}
	
	/* Setting */
	function _insert_table_setting()
	{
		if(Schema::hasTable('setting')){
			if(!DB::table('setting')->where('setting_name','IP_BLOCK_LIST')->first()){
				DB::table('setting')->insert(['setting_name'=>'IP_BLOCK_LIST','setting_value'=>'[]']);
			}
			if(!DB::table('setting')->where('setting_name','IP_WHITELIST')->first()){
				DB::table('setting')->insert(['setting_name'=>'IP_WHITELIST','setting_value'=>'["127.0.0.1"]']);
			}
			if(!DB::table('setting')->where('setting_name','THEME')->first()){
				DB::table('setting')->insert(['setting_name'=>'THEME','setting_value'=>'THEME1']);
			}
			if(!DB::table('setting')->where('setting_name','SKIN')->first()){
				DB::table('setting')->insert(['setting_name'=>'SKIN','setting_value'=>'purple']);
			}
		}
	}
	
	
	
	
	
	
	
	
	
}