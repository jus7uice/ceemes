@extends('layouts.backend')

{{-- Title --}}
@section('title')
	- {{trans('general.tag.tag')}}
@endsection

{{-- Page title --}}
@section('pagetitle')
	{{trans('general.tag.tag')}}
@endsection

@section('content')
	
	<!-- Main content -->
    
      <div class="row">
        
		<div class="col-md-6">		
			<p><a class="btn btn-primary" href="{{url(ADMIN_PATH.'tag.create')}}" data-toggle="ajaxModal" >{{trans('general.label.add_new')}}</a></p>
        </div>
		
		<div class="col-md-12">
			<div class="box box-primary">
				<form role="form" action="{{url(ADMIN_PATH.'tag.delete')}}" method="post" id="ajxFormDelete">
				<div class="box-header with-border">
				  <h3 class="box-title">{{trans('general.tag.tag')}}</h3>
				</div>
				<div class="box-body">
				
					<table id="dataTbl" class="table table-bordered table-hover datatable" data-ajax="{{url(ADMIN_PATH.'tag.data')}}" data-processing="true" data-server-side="true"  data-length-menu="[50,100,250]">
						<thead>
						<tr>
						  <th data-data="chkbox" data-orderable="false" width="50"><input type="checkbox" id="select-all" /></th>
						  <th data-data="name">{{trans('general.tag.name')}}</th>
						  <th data-data="lbl_is_trending">{{trans('general.label.is_trending')}}</th>
						  <th data-data="view_count">{{trans('general.label.view_count')}}</th>
						  <th data-data="status" data-search="false">{{trans('general.label.status')}}</th>
						  <th data-data="action"></th>
						</tr>
						</thead>
						
					</table>
				
				</div>
				 <div class="box-footer">
					<button type="submit" class="btn btn-danger">{{trans('general.button.delete')}}</button>
					 {{csrf_field()}}
				</div>
				</form>	
			</div>
		</div>
      </div>
      <!-- /.row -->

@endsection