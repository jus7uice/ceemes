@extends('layouts.backend')

{{-- Title --}}
@section('title') - {{trans('general.profile.title')}}@endsection

{{-- Page title --}}
@section('pagetitle'){{trans('general.profile.title')}}@endsection

@section('content')
	<!-- Main content -->
    
      <div class="row">
        
		<div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">{{trans('general.profile.summary')}}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{url(ADMIN_PATH.'admin.profile.post')}}" method="post" id="ajxForm" data-ajxForm-reset="false">
              <div class="box-body">
                <div class="form-group">
                  <label>{{trans('general.people.username')}} *</label>
                  <input type="text" class="form-control" name="username" value="{{$user->username}}" />
                </div>
				
				<div class="form-group">
                  <label>{{trans('general.people.name')}} *</label>
                  <input type="text" class="form-control" name="name" value="{{$user->name}}" />
                </div>
				
				<div class="form-group">
                  <label>{{trans('general.people.email')}}</label>
                  <input type="text" class="form-control" name="email" value="{{$user->email}}" />
                </div>
				
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">{{trans('general.button.save')}}</button>
              </div>
			  {{csrf_field()}}
				  {{Form::hidden('id',$auth_admin->id)}}
            </form>
          </div>
          <!-- /.box -->
        </div>
		
		<div class="col-md-6">
			<div class="box box-danger">
				<div class="box-header with-border">
				  <h3 class="box-title">{{trans('general.label.change_password')}}</h3>
				</div>
				
				<form role="form" action="{{url(ADMIN_PATH.'admin.profile.chgpass')}}" method="post" id="">
				<div class="box-body">
				
					<div class="form-group">
					  <label>{{trans('general.label.current_password')}}</label>
					  <input type="password" class="form-control" name="current_password" value="{{old('current_password')}}" />
					</div>
					
					<div class="form-group">
					  <label>{{trans('general.label.new_password')}}</label>
					  <input type="password" class="form-control" name="new_password" value="{{old('new_password')}}" />
					</div>
					
					<div class="form-group">
					  <label>{{trans('general.label.new_password_retype')}}</label>
					  <input type="password" class="form-control" name="new_password_retype" value="{{old('new_password_retype')}}" />
					</div>
				</div>
				
				<div class="box-footer">
					<button type="submit" class="btn btn-success">{{trans('general.button.save')}}</button>
				</div>
				  {{csrf_field()}}
				</form>
			</div>
		</div>
      </div>
      <!-- /.row -->

@endsection