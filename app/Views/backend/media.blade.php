@extends('layouts.backend')

{{-- Title --}}
@section('title')
	- {{trans('general.media.media')}}
@endsection

{{-- Page title --}}
@section('pagetitle')
	{{trans('general.media.image_manager')}}
@endsection

@section('content')
	
	<!-- Main content -->
    
      <div class="row">
        
		<div class="col-md-6">		
			<p><a class="btn btn-primary" href="{{url(ADMIN_PATH.'media.create')}}" data-toggle="ajaxModal" >{{trans('general.label.add_new')}}</a></p>
        </div>
		
		<div class="col-md-12">
			<div class="box box-primary">
				<form role="form" action="{{url(ADMIN_PATH.'media.delete')}}" method="post" id="ajxFormDelete">
				<div class="box-header with-border">
				  <h3 class="box-title">{{trans('general.media.image_manager')}}</h3>
				</div>
				<div class="box-body">				
					<div class="overlay">
					  <i class="fa fa-refresh fa-spin"></i>
					</div>
					
					<div class="ajxPaginationResult">
						@include('backend.media_list')				
					</div>
				
					
				
				</div>
				 <div class="box-footer">
					
				</div>
				</form>	
			</div>
		</div>
      </div>
      <!-- /.row -->

@endsection

@section('css')
<link rel="stylesheet" href="{{asset('plugins/fancybox/jquery.fancybox.css')}}" type="text/css">
@endsection
@section('js')
<script src="{{asset('plugins/fancybox/jquery.fancybox.js')}}"></script>
<script>
$(function(){
	$(".fancybox").fancybox();
	$(".overlay").fadeOut();
	$(document).on('click', '.pagination a', function(e) {
		e.preventDefault();
		$(".overlay").fadeIn();
		var $this = $(this)
		var target = $('.ajxPaginationResult');
		var url = $this.attr('href');
		 $.ajax({ url : url,  success: function(data){
			$(target).html(data); 
			window.history.pushState("", "", url);
			$(".overlay").fadeOut();
			$(".fancybox").fancybox();
		}			 
		}).fail(function () { alert('Permintaan gagal, silahkan coba kembali.'); $(".overlay").fadeOut();});	 
		return false; 
	});
	
});
</script>

@endsection