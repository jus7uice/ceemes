<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'user.edit').'" method="post" id="ajxForm">
  <div class="box-body">
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.label.name').' *</span>
	  <input type="text" class="form-control" name="name" value="'.$item->name.'" />
	</div>

	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.people.email').' *</span>
	  <input type="email" class="form-control" name="email" value="'.$item->email.'" />
	</div>
		
	<hr />
	
	<div class="input-group">
	  <span class="input-group-addon">'.trans('general.people.password').' *</span>
	  <input type="password" class="form-control" name="password">
	</div>
	<span class="help-block text-orange">'.trans('message.comment_change_password').' *</span>
	<hr />
	
	<div class="form-group">
	   '.Form::hidden('status',0).'
		'.Form::checkbox('status',1,($item->status==1)?true:false).'
		 <label>'.trans('general.label.is_active').' *</label>
	</div>
	
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
	   '.Form::hidden('id',Request()->id).'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.edit').' | '. trans('general.user.user'), 'body'=>$body])