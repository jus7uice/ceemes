@extends('layouts.backend')

{{-- Title --}}
@section('title')
	- Dashboard
@endsection

{{-- Page title --}}
@section('pagetitle')
	Dashboard <small></small>
@endsection

@section('content')
	 <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-coffee"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">{{trans('general.tag.tag')}}</span>
              <span class="info-box-number">@widget('WidgetTagCount')</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-gamepad"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Widget 2</span>
              <span class="info-box-number">2000</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Widget 3</span>
              <span class="info-box-number">20000</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Likes</span>
              <span class="info-box-number">93,139</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- =========================================================== -->
	  

	<div class="row">
		<div class="col-sm-4">
		@widget('recentAdminLogs')
		</div>
		<div class="col-sm-4">
		@widget('recentUsers')
		</div>
	</div>
	
	
@endsection