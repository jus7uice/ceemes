@extends('layouts.backend')

{{-- Title --}}
@section('title')
	- {{trans('general.post.post')}}
@endsection

{{-- Page title --}}
@section('pagetitle')
	{{trans('general.post.post')}}
@endsection

@section('content')
	
	<!-- Main content -->
    
      <div class="row">
        
		<div class="col-md-6">		
			<p><a class="btn btn-primary" href="{{url(ADMIN_PATH.'post.create')}}" >{{trans('general.label.add_new')}}</a></p>
        </div>
		
		<div class="col-md-12">
			<div class="box box-primary">
				<form role="form" action="{{url(ADMIN_PATH.'post.delete')}}" method="post" id="ajxFormDelete">
				<div class="box-header with-border">
				  <h3 class="box-title">{{trans('general.post.post')}}</h3>
				</div>
				<div class="box-body">
				
					<table id="dataTbl" class="table table-bordered table-hover datatable" data-ajax="{{url(ADMIN_PATH.'post.data')}}" data-processing="true" data-server-side="true"  data-length-menu="[50,100,250]">
						<thead>
						<tr>
						  <th data-data="chkbox" data-orderable="false" width="50"><input type="checkbox" id="select-all" /></th>
						  <th data-data="id" width="50">{{trans('general.post.id')}}</th>
						  <th data-data="channel.name">{{trans('general.channel.channel')}}</th>
						  <th data-data="title">{{trans('general.post.title')}}</th>
						  <th data-data="lbl_post_tag">{{trans('general.tag.tag')}}</th>
						  <th data-data="view_count">{{trans('general.label.view_count')}}</th>
						  <th data-data="updated_at">{{trans('general.label.updated_at')}}</th>
						  <th data-data="status" data-search="false">{{trans('general.label.status')}}</th>
						  <th data-data="action"></th>
						</tr>
						</thead>
						
					</table>
				
				</div>
				 <div class="box-footer">
					<button type="submit" class="btn btn-danger">{{trans('general.button.delete')}}</button>
					 {{csrf_field()}}
				</div>
				</form>	
			</div>
		</div>
      </div>
      <!-- /.row -->

@endsection