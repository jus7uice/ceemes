<?php 
$body = '
<form role="form" action="'.url(ADMIN_PATH.'restrical.username.create').'" method="post" id="ajxForm">
  <div class="box-body">
	<div class="input-group">
	  <span class="input-group-addon" for="exampleInputEmail1">Restrical Username</span>
	  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Exm.: admin" name="ip_address">
	</div>
  </div>
  <!-- /.box-body -->

  <div class="box-footer">
	<button type="submit" class="btn btn-primary btn-success">'.trans('general.button.save').'</button>
	  '.csrf_field().'
  </div>
</form>
';

?>


@include('modal.modal',['title'=>trans('general.label.add_new'), 'body'=>$body])