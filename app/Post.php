<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'channel_id', 
		'title',
		'synopsis',
		'content',
		'cover',
		'keywords',
		'admin_id',
		'params',
		'status'
    ];
   
	public function channel()
    {
        // return 'Admin Group';
		return $this->belongsTo('\App\Channel', 'channel_id');
    }
	
	
}
