<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use View;

class CheckAuthAdmin
{
    
	/**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (!Auth::guard('admin')->check())
		{
			Auth::logout();	
			Auth::guard('admin')->logout();	
			
			if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
				return redirect()->intended(ADMIN_PATH)->withErrors(trans('message.access_denied'));   
            }
		}
		
		$user = Auth::guard('admin')->user();
		/* cek 1 device */
		$login_session = session('login_session');
		if($login_session != $user->login_session){
			Auth::logout();	
			Auth::guard('admin')->logout();
			session()->flush();
			return redirect()->intended(ADMIN_PATH)->withErrors(trans('message.login_other_device_detected'));   
		}		
		
		View::share('auth_admin', json_decode(json_encode(Auth::guard('admin')->user())));
		$request->attributes->add(['auth_admin' => json_decode(json_encode(Auth::guard('admin')->user()))]);
		
		return $next($request);
    }
}
