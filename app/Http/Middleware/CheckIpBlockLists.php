<?php

namespace App\Http\Middleware;

use Closure;
use DB;
// use App\Setting;

class CheckIpBlockLists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if(!$request->session()->has('checkIpBlock'))
		{
			$checkIpBlock = DB::table('setting')->where('setting_name','IP_BLOCK_LIST')->pluck('setting_value')->first();
			/* Set global Session */
			$request->session()->put('checkIpBlock',json_decode($checkIpBlock));
		}		
		$checkIpBlockList = session('checkIpBlock');
		if(in_array($request->ip(),$checkIpBlockList))
		{
			return redirect('err/ip.blocked');
		}	
		return $next($request);
    }
}
