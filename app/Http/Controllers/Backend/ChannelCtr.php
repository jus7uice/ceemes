<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use Validator;
use Illuminate\Validation\Rule;
use Datatables;

use App\Channel;

class ChannelCtr extends Controller
{
	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('backend.channel');
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {		
		$rows = Channel::where('channels.status','<',2)->select('channels.*');
        return Datatables::of($rows)
		->addColumn('chkbox',function($row){
			return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
		})
		->addColumn('lbl_parent',function($row){
			if($row->parent_id > 0)	return $row->parent->name; else return ' - ';
		})
		->addColumn('lbl_is_top',function($row){
			if($row->is_top > 0)	return '<span class="label label-success">Yes</span>';
			else return '<span class="label label-danger">No</span>';
		})
		->addColumn('status',function($row){
			if($row->status == 1)	return '<span class="label label-success">Active</span>';
			else return '<span class="label label-danger">Not Active</span>';
		})
		->addColumn('action',function($row){
			$action = '
				<a href="'.url(ADMIN_PATH.'channel.edit?id='.$row->id).'" data-toggle="ajaxModal">'.trans('general.button.edit').'</a>
			
			';
			return $action;
		})
		->rawColumns(['chkbox','lbl_parent','lbl_is_top','status','action'])
		->make();
    }
	
	
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
		/* Load by ajax only */
		if(!$request->ajax()){return redirect('err/restrical.access');}
		
		$channel = Channel::where('status','<',2)->pluck('name','id')->toArray();
		$channelList = ['0'=>'Select Parent Channel :'] + $channel;
		return view('backend.channel_create',compact('channelList'));
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'name' => 'required|unique:channels,name',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		// dd($request->all());
		/* Save to DB */
		$row = new Channel;
		$row->fill($request->all());
		$row->save();
				
		// /* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	/**
     * Edit a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEdit(Request $request)
    {
		/* Load by ajax only */
		if(!$request->ajax()){return redirect('err/restrical.access');}
		
		$item = Channel::find($request->id);
		$channel = Channel::where('status',1)->pluck('name','id')->toArray();
		$channelList = ['0'=>'Select Parent Channel :'] + $channel;
		return view('backend.channel_edit',compact('item','channelList'));
    }
	
	/**
     * Edit a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'name' => [
				'required',
				Rule::unique('channels')->ignore($request->id),
			],
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		// dd($request->all());
		/* Save to DB */
		$row = Channel::find($request->id);
		$row->fill($request->all());
		$row->save();
				
		// /* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	 /**
     * Delete resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postDelete(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'deleteItems' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		
		/* If group exist */
		DB::table('channels')->whereIn('id',$request->deleteItems)->update(['status'=>3]);
		
		/* Response */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.delete.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.delete.success'));	
    }
	
}
