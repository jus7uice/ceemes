<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use DB;
use Hash;
use Validator;
use Illuminate\Validation\Rule;

use Datatables;

use App\Admin;
use App\AdminGroup;

class AdminCtr extends Controller
{	
   
	/**
     * Display my profile
     *
     * @return \Illuminate\Http\Response
     */
    public function getMe()
    {
        $user = Auth::guard('admin')->user();
		return view('backend.admin_profile',compact('user'));
    }

    /**
     * postMe a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postMe(Request $request)
    {
		$user = Auth::guard('admin')->user();

		/* Validate */
		$validator = Validator::make($request->all(), [
			'username' => [
				'required',
				Rule::unique('users')->ignore($request->id),
			],
			'name' => 'required',
			// 'email' => [
				// 'email',
				// 'required',
				// Rule::unique('admin')->ignore($request->id),
			// ],
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
			
		/* Save update */
		$update = [
			'username'=>$request->username,
			'name'=>$request->name,
			'email'=>$request->email,
			'updated_at'=>date("Y-m-d H:i:s"),
		];
		DB::table('admin')->where('id',$user->id)->update($update);
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans("message.update.success"));
		
    }
	
	 /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postChangePassword(Request $request)
    {
		$user = Auth::guard('admin')->user();
		/* Validate */
		$request->validate([
			'current_password' => 'required|min:3|max:255',
			'new_password' => 'required|min:3|max:255',
			'new_password_retype' => 'required|min:3|max:255',
		]);
		
		/* Next Validate */
		if($request->new_password != $request->new_password_retype){
			return redirect()->back()->withInput()->withErrors(trans("message.new_password_match_wrong"));
		}
		else if(Hash::check($request->current_password, $user->password))
		{
			DB::table('admin')->where('id',$user->id)->update(['password'=> Hash::make($request->new_password)]);
			return redirect()->back()->with('msg',trans("message.update.success"));
		}
		else return redirect()->back()->withInput()->withErrors(trans("message.current_password_wrong"));
    }
	
	
	/* ================================================================================================================================================================== */
	
	
	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUsersData(Request $request)
    {		
		/* Load by ajax only */
		if(!$request->ajax()){return die('restrical.access');}
		
		$users = Admin::with('group')->where('admin.status',1)->select('admin.*');
        return Datatables::of($users)
		->addColumn('chkbox',function($row){
			if($row->is_superadmin <= 0) return '<input type="checkbox" name="deleteItems[]" value="'.$row->id.'" />';
			else return '';
		})
		->addColumn('lbl_sa',function($row){
			if($row->is_superadmin > 0)	return '<span class="label label-success">Yes</span>';
			else return '<span class="label label-danger">No</span>';
		})
		->addColumn('action',function($row){
			$action = '
				<a href="'.url(ADMIN_PATH.'admin.user.edit?id='.$row->id).'" data-toggle="ajaxModal">'.trans('general.button.edit').'</a>
			
			';
			return $action;
		})
		->rawColumns(['chkbox','lbl_sa','action'])
		->make();
    }
	
   
	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUsers()
    {
		return view('backend.admin_user');
    }
		
	/**
     * Create a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createUser(Request $request)
    {
		/* Load by ajax only */
		if(!$request->ajax()){return redirect('err/restrical.access');}
		
		$groupList = [''=>'Select Group:'] + AdminGroup::where('status',1)->pluck('name','id')->toArray();
		return view('backend.admin_user_create',compact('groupList'));
    }
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreateUser(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'username' => 'required|min:3',
			'password' => 'required|min:3',
			'name' => 'required',
			// 'email' => 'required|email',
			'group' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		// dd($request->all());
		/* Save to DB */
		$row = new Admin;
		// $row->fill($request->all());
		$row->password = Hash::make($request->password);
		$row->username = $request->username;
		$row->name = $request->name;
		$row->email = $request->email;
		$row->admin_group_id = $request->group;
		$row->save();
		
		/* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
    }

	/**
     * Edit a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editUser(Request $request)
    {	
		/* Load by ajax only */
		if(!$request->ajax()){return redirect('err/restrical.access');}
		
		/* get resource */
		$item = Admin::find($request->id);
		$groupList = [''=>'Select Group:'] + AdminGroup::where('status',1)->pluck('name','id')->toArray();
		return view('backend.admin_user_edit',compact('item','groupList'));
    }
	
	
	/**
     * Create a resource : POST.
     *
     * @return \Illuminate\Http\Response
     */
    public function postEditUser(Request $request)
    {
		/* Validate */
		$validator = Validator::make($request->all(), [
			'username' => 'required|min:3',
			'name' => 'required',
			// 'email' => 'required|email',
			'group' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->all()]);
			}
		}
		// dd($request->all());
		/* Save to DB */
		$row = Admin::find($request->id);
		// $row->fill($request->all());		
		if($request->has('password') && strlen($request->password)>2){
			$row->password = Hash::make($request->password);
		}		
		$row->username = $request->username;
		$row->name = $request->name;
		$row->email = $request->email;
		$row->admin_group_id = $request->group;
		$row->save();
		
		/* Redirc */
		if($request->ajax()){
			return response()->json(['message'=>[trans('message.save.success')]]);
		}
		return redirect()->back()->with('msg',trans('message.save.success'));
    }
	
	
	
	 /**
     * Delete user resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postDeleteUsers(Request $request)
    {		
		/* Validate */
		$validator = Validator::make($request->all(), [
			'deleteItems' => 'required',
		]);
		
		/* Ajax Response Validate */
		if($request->ajax()){
			if (!$validator->passes()) {
				//return response('Unauthorized.', 401);
				return response()->json(['error'=>$validator->errors()->first()]);
			}
		}
		
		/* If admin/user exist */
		$message = []; $error = [];
		$fetch = DB::table('admin')->whereIn('id',$request->deleteItems)->get();
		foreach ($fetch as $row){
			if($row->is_superadmin == 1)
			{
				$error[] = trans('message.delete.error');
			}
			else
			{
				$fetch = DB::table('admin')->where('id',$row->id)->update(['status'=>0]);
				$message[] = trans('message.delete.success');
			}
		}
		
		/* Response */
		if($request->ajax()){
			return response()->json(['error'=>$error,'message'=>$message]);
		}
		return redirect()->back()->with('msg',trans('message.update.success'));	
    }

	
}
