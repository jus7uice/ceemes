<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Admin;

class ApplicationCtr extends Controller
{
     /**
     * Dashboard
     * @return Response
     */
	 
	function index(Request $request)
	{
		$ag = Admin::find(1);
		return view('backend.dashboard');
	}
}
