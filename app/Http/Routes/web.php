<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/* BAckend Route */
require '_backend.php';

Route::group(['prefix' => 'err'], function () {
	Route::get('ip.blocked',function(){return view('special.err_ip_block');});
	Route::get('restrical.access',function(){return view('special.err_restrical_access');})->middleware('auth.admin');
});