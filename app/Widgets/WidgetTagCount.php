<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Tag;

class WidgetTagCount extends AbstractWidget
{
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Tag::count();
		return (string) $count;
    }
}