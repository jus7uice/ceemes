<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use DB;

class RecentUsers extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
     protected $config = [
		'count'=>5,
	];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //
		$data = DB::table('users')->where('status',1)->orderBy('id','DESC')->take($this->config['count'])->get();
        return view('widgets.recent_users', [
            'data' => $data,
        ]);
    }
}
